const express = require("express");
const cors = require("cors");
const app = express();
const PORT = 5001;
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
app.use('/api/book', require('./webapp/api/book'));
app.listen(PORT, () => console.log(`Server started on ${PORT}`));
 