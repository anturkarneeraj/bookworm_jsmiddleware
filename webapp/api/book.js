const express = require('express');
var sql = require('./../common/db');
const router = express.Router();

router.get('/:like', function(request,response){
    var like = request.params.like;
    // console.log(request.body.like);
    if(like == undefined){
        like = "%";
    }else if(like.length == 0){
        like = "%"
    }
    var sqlQuery = "CALL show_books('%" + like + "%');";
    
    sql.query(sqlQuery, function (err, res) {
    var result = {};
    if(err) {
     
        result['data'] = res;
        result['success'] = true;
        result['message'] = err;
        console.log("error: ", err);
        response.json(result); 
    }
    else{
        
        result['data'] = res;
        result['success'] = true;
        result['message'] = "Books fetched successfully";
        response.json(result); 
     
    }
    }); 
});

router.post('/', function(request,response){
    var type = request.body.type;
    var isbn = request.body.isbn;
    var title = request.body.title;
    var cnt = request.body.cnt;
    var e_author = request.body.e_author;
    var n_author = request.body.n_author;

    var subtitle = request.body.subtitle;
    var publication = request.body.publication;
    var description = request.body.description;
    var published_date = request.body.published_date;
    var page_count = request.body.page_count;
    var language = request.body.language;
    var thumbnail = request.body.thumbnail;

    var sqlQuery = "CALL add_book("+ type +",'" + title + "','" + subtitle + "','"
                                    + publication + "','" + description + "','"
                                    + published_date + "'," + page_count + ",'"
                                    + language + "','" + thumbnail + "','"
                                    + isbn + "','" + cnt + "','"
                                    + e_author + "','" + n_author + "');";
  
    sql.query(sqlQuery, function (err, res) {
        var result = {};
        if(err) {
            
            result['data'] = res;
            result['success'] = true;
            result['message'] = err;
            console.log("error: ", err);
            response.json(result); 
        }
        else{
            
            result['data'] = res;
            result['success'] = true;
            result['message'] = "Books added successfully";
            response.json(result); 
            
        }
    }); 
    
});

module.exports = router;